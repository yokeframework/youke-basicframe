<?php
namespace app\index\model;

use app\common\model\Base;
use think\model\concern\SoftDelete;

class User extends Base
{
    use SoftDelete;
    
    protected $deleteTime = 'delete_time';
    protected $pk = 'user_id';
    
}