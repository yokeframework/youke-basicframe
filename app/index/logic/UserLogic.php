<?php
namespace app\index\logic;

use app\index\model\User;
use base\BaseLogic;

class UserLogic extends BaseLogic
{
    /**
     * 修改密码
     *
     * @param int $user_id 用户ID
     * @param string $new_pass 新密码
     * @param string $old_pass 原密码
     * @return void
     */
    public function resetPass($user_id,$new_pass,$old_pass)
    {
        $user = User::find($user_id);

        if(!$user){
            return_error(['info'=>'用户不存在!']);
        }

        if(sha1($old_pass.$user->salt)!=$user->password){
            return_error(['info'=>'原密码错误']);
        }

        $user->password = sha1($new_pass.$user->salt);

        return $user->save();
    }

    /**
     * 修改客户资料
     *
     * @param int $user_id 用户ID
     * @param array $data
     * @return void
     */
    public function updateUser($user_id,$data=[])
    {
        if(empty($data)){
            return true;
        }

        $user = User::find($user_id);

        if(!$user){
            return_error(['info'=>'用户不存在!']);
        }

        return $user->allowField(['user_name'])->save($data);
    }

    /**
     * 获取用户详细信息
     *
     * @param int $user_id
     * @return void
     */
    public function getUserInfo($user_id)
    {
        $user = User::find($user_id);

        if(!$user){
            return_error(['info'=>'用户不存在!']);
        }

        return $user->toArray();
        
    }

}