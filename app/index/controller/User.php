<?php
namespace app\index\controller;

use app\index\logic\UserLogic;
use service\LoginService;

/**
 * 测试
 * 
 * @TokenExcept(login)
 * @ApiCat(测试1)
 * 
 * @name  你好
 */
class User extends Base
{
    protected $middleware = ['CheckToken'=>['except'=>'login']];
    
    /**
     * 用户登录
     * 
     * @Route('/login',post)
     * @Req(login_name|1|登录名,password|1|密码)
     * @param LoginService $login
     * @return void
     */
    public function login(LoginService $login)
    {
        $param = $this->request->param();

        $this->validate($param,['login_name|登录名'=>'require','password|密码'=>"require"]);

        $res = $login->setModel('User')->getToken();

        return return_success(['data'=>['token'=>$res['token'],'login_name'=>$res['login_name'],'last_login'=>$res['last_login'],'user_name'=>$res['user_name']]]);
        
    }

    /**
     * 修改密码
     * 
     * @Route('/user/resetpass',post)   
     * @Req(new_pass|1|新密码,old_pass|0|原密码)
     * 
     * @return void
     */
    public function resetPass()
    {
        $param = $this->request->param();

        $this->validate($param,['new_pass|新密码'=>'require','old_pass|原密码'=>"require"]);

        $user = new UserLogic();

        $res = $user->resetPass($this->request->loginUser['user_id'],input('new_pass'),input('old_pass'));

        if($res){
            return return_success();
        }

        return_error(['info'=>'修改密码失败']);
    }

    /**
     * 修改用户资料
     *
     * @Route('/user/update',post)
     * @Req(user_id|1|用户ID,user_name|0|用户名)
     * @return void
     */
    public function updateUser()
    {
        $param = $this->request->param();

        $user = new UserLogic();

        $res = $user->updateUser($this->request->loginUser['user_id'],$param);

        if($res){
            return return_success();
        }

        return_error(['info'=>'修改资料失败']);

    }

    /**
     * 获取用户详细信息
     *
     * @Route('/user/getinfo',get)
     * @ApiCat(测试2)
     * 
     * @return void
     */
    public function getUserInfo()
    {
        $user = new UserLogic();

        $res = $user->getUserInfo($this->request->loginUser['user_id']);

        return return_success(['data'=>$res]);
    }



}