<?php
namespace app\index\controller;

use base\BaseController;

class Base extends BaseController
{
    protected $middleware = ['CheckToken'];
}