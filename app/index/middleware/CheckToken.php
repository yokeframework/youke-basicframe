<?php
namespace app\index\middleware;

class CheckToken
{
    private $ForbiddenRoute=["1"=>[],"2"=>['User/addUser','User/updateUser','User/resetPass','User/updatePass','User/changeStatus','Customer/addCustomer','Customer/updateCustomer','User/getList','Customer/assign','Customer/delete','User/delUser']];

    public function handle($request, \Closure $next)
    {

        $request->loginUser = app('Login_service')->setModel('User')->checkToken();

        $path = $request->controller().'/'.$request->action();

        if(in_array($path,$this->ForbiddenRoute[$request->loginUser['rank_id']])){

            return_error(['info'=>'此操作无权限!']);

        }

        return $next($request);
    }
}