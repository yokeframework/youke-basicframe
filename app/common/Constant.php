<?php
/**
 * 全局状态常量，定义全局常量应对后续字段状态变更
 */
namespace app\common;


class Constant
{
    const SUPER_ADMIN = 1; //超级管理员ID
    const SUPER_ROLE = 1;//超级角色ID
    const SUPER_ADMIN_TYPE = 1; //内置管理员
    const SUPER_ROLE_TYPE = 1;//内置角色
}