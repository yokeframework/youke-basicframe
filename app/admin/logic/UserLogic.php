<?php
namespace app\admin\logic;

use app\admin\model\Exam;
use app\admin\model\Goods;
use app\admin\model\Order;
use app\admin\model\User;
use app\admin\model\UserExercise;
use app\admin\model\UserWrong;
use base\BaseLogic;

class UserLogic extends BaseLogic
{
    /**
     * 添加学员
     *
     * @param array $data
     * @return void
     */
    public function addUser($data)
    {

        $user = new User();

        $data['salt'] =  mt_rand(1000,9999);

        $data['password'] = sha1($data['password'].$data['salt']);

        $res = $user->save($data);

        if($res){
            return $user->user_id;
        }

        return_error(['info'=>'添加学员失败']);
    }

    /**
     * 更新学员信息
     *
     * @param [type] $user_id
     * @param [type] $data
     * @return void
     */
    public function updateUser($user_id,$data)
    {
        $user = User::find($user_id);

        if(!$user){
            return_error(['info'=>'学员不存在']);
        }

        $res = $user->allowField(['user_name','mobile','identity_card','rank_id','status','group_id'])->save($data);

        if($res){
            return true;
        }

        return_error(['info'=>'更新失败!']);

    }

    /**
     * 删除学员
     *
     * @param [type] $user_id
     * @return void
     */
    public function deleteUser($user_id)
    {
        $user = User::find($user_id);

        if(!$user){
            return_error(['info'=>"学员不存在"]);
        }

        $res = $user->delete();

        if($res){
            return true;
        }

        return_error(['info'=>'删除失败']);
    }

    /**
     * 获取学员列表
     *
     * @param [type] $param
     * @param [type] $is_page
     * @return void
     */
    public function getUserList($param, $is_page)
    {
        $wconfig=[
            'status'=>['status','='],
            'rank_id'=>['rank_id','='],
            'group_id'=>['group_id','='],
            'mobile'=>['mobile','like'],
            'user_name'=>['user_name','like'],
        ];
        $user = new User();

        $user = $this->buildwhere($user,$param,$wconfig);

        if($is_page){
            $data['list_rows']=$param['list_rows']??"15";
            $data['page']=$param['page']?? "1";
            $list = $user->paginate($data);
        }else{
            $list = $user->select();
        }

        return $list;
    }   

}