<?php
use think\facade\Route;

Route::allowCrossDomain(["Access-Control-Allow-Headers"=>"token,Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, X-CSRF-TOKEN, X-Requested-With"]);

Route::post('login', 'admin/login');
Route::get('logininfo','admin/getLoginInfo');//登录人详情

/**账户管理 */
Route::post('admin/add','admin/addAdmin');
Route::post('admin/update','admin/updateAdmin');
Route::post('admin/del','admin/deleteAdmin');
Route::post('admin/updatepass','admin/updatePass');
Route::post('admin/resetpass','admin/resetPass');
Route::post('admin/status','admin/statusChange');//启用禁用
Route::get('admin/list','admin/getAdminList');
Route::get('admin/info','admin/getAdminInfo');
Route::get('/log/config','Admin/getLogConfigList');//获取日志配置列表
Route::get('/log/list','Admin/getLogList');//获取日志配置列表

/**角色管理 */
Route::post('role/add','role/addRole');
Route::post('role/update','role/updateRole');
Route::post('role/del','role/deleteRole');
Route::post('role/status','role/statusChange');//删除角色
Route::get('role/list','role/getRoleList');
Route::get('role/info','role/getRoleInfo');


/**学员管理 */
Route::post('user/add','user/addUser');
