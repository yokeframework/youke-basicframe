<?php
namespace app\admin\model;

use app\common\model\Base;
use think\model\concern\SoftDelete;

class User extends Base
{
    use SoftDelete;
    
    protected $deleteTime = 'delete_time';
    protected $pk = 'user_id';

    public function group()
    {
        return $this->hasOne('Group','group_id')->bind(['group_name']);
    }

    public function rank()
    {
        return $this->hasOne('Rank','rank_id')->bind(['rank_name']);
    }
    
}