<?php
namespace app\admin\controller;

use base\BaseController;

/**
 * 基础controller
 */
class Base extends BaseController
{
    protected $middleware = ['CheckToken'];
}