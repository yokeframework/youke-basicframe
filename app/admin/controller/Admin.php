<?php
namespace app\admin\controller;

use app\admin\logic\AdminLogic;
use service\LoginService;

/**
 * 管理员接口
 * 
 * @TokenExcept(login)
 * @ApiCat(管理员管理)
 * 
 */
class Admin extends Base
{
    protected $middleware = ['CheckToken'=>['except'=>'login']];
    
    /**
     * 登录
     * 
     * @Route('/login',post)
     * @Req(login_name|1|登录名,password|1|密码)
     * @ApiCat(公共分类)
     * @param LoginService $login
     * @return void
     */
    public function login(LoginService $login)
    {
        $param = $this->request->param();

        $this->validate($param,['login_name|登录名'=>'require','password|密码'=>"require",'captcha|验证码'=>'require|captcha']);

        $res = $login->setModel('app\model\Admin','priv')->getToken();//调用登录服务

        if(!$res['role_id']||($res['role_status'] && $res['role_status']==0)){
            return_error(['info'=>'角色已禁用，请联系管理员!']);
        }

        return return_success(['data'=>['token'=>$res['token'],'login_name'=>$res['login_name'],'last_login'=>$res['last_login'],'admin_name'=>$res['admin_name'],'menu'=>$res['menu']]]);
        
    }
    
    /**
     * 添加管理员
     *
     * @Route('/admin/add',post)
     * @Req(login_name|1|登录名,password|1|密码,mobile|0|手机号)
     * @return void
     */
    public function addAdmin()
    {
        $param = $this->request->param();

        $this->validate($param,['login_name|登录名'=>'require|unique:app\model\admin','password|密码'=>"require",'admin_name|姓名'=>'require|unique:app\model\admin']);
        
        $admin = new AdminLogic();

        $param['create_admin'] = $this->request->loginUser['admin_id'];

        $res = $admin->addAdmin($param);

        if($res){
            return return_success();
        }
    }

    /**
     * 更新管理员
     *
     * @Route('/admin/update',post)
     * @Req(admin_id|1|管理员ID,admin_name|0|姓名,mobile|0|手机号,role_id|0|角色ID)
     * @return void
     */
    public function updateAdmin()
    {
        $param = $this->request->param();

        $this->validate($param,['admin_id|管理员ID'=>'require']);
        
        $admin = new AdminLogic();

        unset($param['admin_id']);

        $res = $admin->updateAdmin(input('admin_id'),$param);

        if($res){
            return return_success();
        }
    }

     /**
     * 启用禁用
     *
     * @Route('/admin/status',post)
     * @Req(admin_id|1|管理员ID,status|1|状态 1启用 0禁用)
     * @return void
     */
    public function statusChange()
    {
        $param = $this->request->param();

        $this->validate($param,['admin_id|管理员ID'=>'require','status|状态'=>'require']);
        
        $admin = new AdminLogic();

        $res = $admin->updateAdmin(input('admin_id'),['status'=>input('status')]);

        if($res){
            return return_success();
        }
    }


    /**
     * 删除管理员
     *
     * @Route('/admin/del',post)
     * @Req(admin_id|1|管理员ID)
     * @return void
     */
    public function deleteAdmin()
    {
        $param = $this->request->param();

        $this->validate($param,['admin_id|管理员ID'=>'require']);
        
        $admin = new AdminLogic();

        $res = $admin->deleteAdmin(input('admin_id'));

        if($res){
            return return_success();
        }
    }

    /**
     * 修改密码
     *
     * @Route('/admin/updatepass',post)
     * @Req(admin_id|1|管理员ID,new_pass|1|新密码,old_pass|1|原密码)
     * @return void
     */
    public function updatePass()
    {
        $param = $this->request->param();

        $this->validate($param,['new_pass|新密码'=>'require','old_pass|原密码'=>'require']);
        
        $admin = new AdminLogic();

        $res = $admin->updatePass($this->request->loginUser['admin_id'],input('new_pass'),input('old_pass'));

        if($res){
            return return_success();
        }
    }

    /**
     * 
     * 重置密码
     *
     * @Route('/admin/resetpass',post)
     * @Req(admin_id|1|管理员ID)
     * @return void
     */
    public function resetPass()
    {
        $param = $this->request->param();

        $this->validate($param,['admin_id|管理员ID'=>'require']);
        
        $admin = new AdminLogic();

        $res = $admin->resetPass(input('admin_id'),input('password',123456));

        if($res){
            return return_success();
        }
    }

    /**
     * 获取管理员列表
     * 
     * @Route('/admin/list',get)
     * @Req(status|0|状态,role_id|0|角色,admin_name|0|管理员名称,mobile|0|手机号)
     * @return void
     */
    public function getAdminList()
    {
        $param = $this->request->param();

        $admin = new AdminLogic();

        $list = $admin->getAdminList($param,input('is_page',0));

        return return_success(['data'=>$list]);

    }

    /**
     * 获取管理员详细信息
     *
     * @Route('/admin/info',get)
     * @Req(admin_id|1|管理员ID)
     * @param [type] $admin_id
     * @return void
     */
    public function getAdminInfo()
    {
        
        $param = $this->request->param();

        $this->validate($param,['admin_id|管理员ID'=>'require']);

        $admin = new AdminLogic();

        $info = $admin->getAdminInfo(input('admin_id'));

        return return_success(['data'=>$info]);
        
    }
    
    /**
     * 获取当前登录人信息
     *
     * @Route('/logininfo',get)
     * @ApiCat(公共分类)
     * @return void
     */
    public function getLoginInfo()
    {

        $admin = new AdminLogic();

        $info = $admin->getAdminInfo($this->request->loginUser['admin_id']);

        return return_success(['data'=>$info]);
    }

    /**
     * 获取日志配置列表
     * 
     * @Route('/log/config',get)
     * @Req(type|0|配置类型 app,module,type)
     * @return void
     */
    public function getLogConfigList()
    {
        $param = $this->request->param();

        $this->validate($param,['type|类型'=>'require|in:app,module,type']);

        $list = (new AdminLogic())->getLogConfig($param['type']);

        return return_success(['data'=>$list]);
    }

    /**
     * 获取日志配置列表
     * 
     * @Route('/log/list',get)
     * @Req(create_time|0|时间段,app|0|终端,module|0|模块,type|0|操作类型,title|0|日志标题)
     * @return void
     */
    public function getLogList()
    {
        $param = $this->request->param();

        $list = (new AdminLogic())->getLogList($param,input('is_page',1));

        return return_success(['data'=>$list]);
    }
}