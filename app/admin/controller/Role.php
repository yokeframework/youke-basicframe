<?php
namespace app\admin\controller;

use app\admin\logic\AdminLogic;

/**
 * 角色管理
 */
class Role extends Base
{   
    /**
     * 添加职位（角色）
     * 
     * @Route('/role/add',post)
     * @Req(role_name|1|角色名,role_desc|1|描述,api|1|接口权限,menu|1|菜单权限)
     * @return void
     */
    public function addRole()
    {
        $param = $this->request->param();

        $this->validate($param,['role_name|职位名'=>'require|unique:role','menu|菜单权限'=>"require"]);
        
        $admin = new AdminLogic();

        $param['create_admin'] = $this->request->loginUser['admin_id'];

        $res = $admin->addRole($param);

        if($res){
            return return_success();
        }
    }

    /**
     * 更新职位名(角色)
     *
     * @Route('/role/update',post)
     * @Req(role_id|1|角色ID,role_name|0|角色名,role_desc|0|描述,api|0|接口权限,menu|0|菜单权限)
     * @return void
     */
    public function updateRole()
    {
        $param = $this->request->param();

        $this->validate($param,['role_id|职位ID'=>'require']);
        
        $admin = new AdminLogic();

        unset($param['role_id']);

        $res = $admin->updateRole(input('role_id'),$param);

        if($res){
            return return_success();
        }
    }

    /**
     * 删除角色
     *
     * @Route('/role/del',post)
     * @Req(role_id|1|角色ID)
     * @return void
     */
    public function deleteRole()
    {
        $param = $this->request->param();

        $this->validate($param,['role_id|职位ID'=>'require']);
        
        $admin = new AdminLogic();

        $res = $admin->deleteRole(input('role_id'));

        if($res){
            return return_success();
        }
    }

    /**
     * 获取角色列表
     *
     * @Route('/role/list',get)
     * @Req(role_name|0|角色名,status|0|状态)
     * @return void
     */
    public function getRoleList()
    {
        $param = $this->request->param();

        $admin = new AdminLogic();

        $list = $admin->getRoleList($param,input('is_page',0));

        return return_success(['data'=>$list]);
    }

     /**
     * 获取角色详细信息
     *
     * @Route('/role/info',get)
     * @Req(role_id|1|角色ID)
     * @return void
     */
    public function getRoleInfo()
    {
        $param = $this->request->param();

        $this->validate($param,['role_id|管理员ID'=>'require']);

        $admin = new AdminLogic();

        $info = $admin->getRoleInfo(input('role_id'));

        return return_success(['data'=>$info]);
        
    }

    /**
     * 启用禁用
     *
     * @Route('/role/status',post)
     * @Req(role_id|1|角色ID,status|1|状态 0禁用 1启用)
     * @return void
     */
    public function statusChange()
    {
        $param = $this->request->param();

        $this->validate($param,['role_id|角色ID'=>'require','status|状态'=>'require']);
        
        $admin = new AdminLogic();

        $res = $admin->updateRole(input('role_id'),['status'=>input('status')]);

        if($res){
            return return_success();
        }
    }

}