<?php
namespace app\admin\controller;

use app\admin\logic\UserLogic;

class User extends Base
{
    /**
     * 添加用户
     *
     * @return void
     */
    public function addUser()
    {
        $param = $this->request->param();

        $this->validate($param,['login_name|登录名'=>'require','password|密码'=>"require",'user_name|用户名'=>'require']);
        
        $user = new UserLogic();

        $param['create_admin'] = $this->request->loginUser['admin_id'];

        $res = $user->addUser($param);

        if($res){
            return return_success();
        }
    }
}