<?php
namespace app\admin\middleware;


/**
 * 登录及接口权限校验
 */
class CheckToken
{

    public function handle($request, \Closure $next)
    {

        $request->loginUser = app('Login_service')->setModel('app\model\Admin','priv')->checkToken();

        if($request->loginUser['status']==0){
            return_error(['info'=>'账号已禁用，请联系管理员!','status'=>1001]);
        }

        if(!isset($request->loginUser['role_status'])){
            return_error(['info'=>'角色已禁用，请联系管理员!','status'=>1001]);
        }

        if($request->loginUser['role_status']==0){
            return_error(['info'=>'角色已禁用，请联系管理员!','status'=>1001]);
        }

        return $next($request);
    }
}