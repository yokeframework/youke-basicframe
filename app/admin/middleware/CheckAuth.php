<?php
namespace app\admin\middleware;

use app\common\Constant;

/**
 * 登录及接口权限校验
 */
class CheckAuth
{
    private $pubapi = ['/admin/exam/getschedulelist'];//公共接口

    public function handle($request, \Closure $next)
    {
        $path = $request->controller().'/'.$request->action();

        $path ='/admin/'.strtolower($path);

        if($request->loginUser['admin_id']!=Constant::SUPER_ADMIN && !in_array($path, $this->pubapi)){
            
            if(!$request->loginUser['api']||!in_array($path,json_decode($request->loginUser['api'],true))){

                return_error(['info'=>'此操作无权限!']);

            }
        }

        return $next($request);
    }
}