<?php
namespace app\model;

use app\common\model\Base;
use think\model\concern\SoftDelete;

class Log extends Base
{
    use SoftDelete;

    protected $deleteTime = 'delete_time';
    // 设置字段信息
    protected $schema = [
        "log_id"=>"int(11)",
        "title"=>"varchar(255)",
        "api"=>"varchar(255)",
        "param"=>"text",
        "ip"=>"varchar(255)",
        "user_agent"=>"varchar(255)",
        "module"=>"varchar(255)",
        "app"=>"varchar(255)",
        "operator_id"=>"int(11)",
        "operator_type"=>"varchar(255)",
        "operator_name"=>"varchar(255)",
        "content"=>"text",
        "before"=>"text",
        "before_data"=>"text",
        "after"=>"text",
        "after_data"=>"text",
        "type"=>"tinyint(4)",
        "code"=>"int(11)",
        "create_time"=>"datetime",
    ];
    //设置主键
    protected $pk = 'log_id';
    
}