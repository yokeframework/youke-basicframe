<?php
namespace app\model;

use app\common\model\Base;
use think\model\concern\SoftDelete;

class Admin extends Base
{
    use SoftDelete;

    protected $deleteTime = 'delete_time';
    // 设置字段信息
    protected $schema = [
        "admin_id"=>"int(11)",
        "login_name"=>"varchar(255)",
        "password"=>"varchar(255)",
        "salt"=>"varchar(255)",
        "admin_name"=>"varchar(255)",
        "mobile"=>"varchar(255)",
        "role_id"=>"int(11)",
        "token"=>"varchar(255)",
        "expire"=>"int(11)",
        "status"=>"tinyint(4)",
        "type"=>"tinyint(4)",
        "create_admin"=>"int(11)",
        "create_admin_name"=>"varchar(255)",
        "last_login"=>"datetime",
        "create_time"=>"datetime",
        "update_time"=>"datetime",
        "delete_time"=>"datetime",
    ];
    //设置主键
    protected $pk = 'admin_id';

    public function role(){
        return $this->belongsTo('role','role_id','role_id')->bind(['role_name']);
    }

    public function priv(){
        return $this->belongsTo('role','role_id','role_id')->bind(['role_name','menu','api','role_status'=>'status']);
    }
    
}