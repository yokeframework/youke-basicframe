<?php
namespace app\model;

use app\common\model\Base;
use think\model\concern\SoftDelete;

class Role extends Base
{
    use SoftDelete;

    protected $deleteTime = 'delete_time';
    // 设置字段信息
    protected $schema = [
        "role_id"=>"int(11)",
        "role_name"=>"varchar(255)",
        "role_desc"=>"varchar(255)",
        "menu"=>"longtext",
        "api"=>"longtext",
        "status"=>"tinyint(4)",
        "create_time"=>"datetime",
        "update_time"=>"datetime",
        "delete_time"=>"datetime",
    ];
    //设置主键
    protected $pk = 'role_id';
    
}